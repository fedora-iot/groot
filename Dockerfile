# This is a simple Dockerfile to demonstrate how to use groot in a container.
# The image is not necessarily useful on itself other than to test that it works.
FROM fedora:34
ARG user=grootuser
RUN dnf install -y gawk make wget tar bzip2 gzip gcc glibc-devel which \
     less strace gdb procps-ng fuse-devel libcap-devel \
     && dnf update -y \
     && dnf clean all \
     && rm -rf /var/cache/yum
# Fix up file capabilities lost in base image
RUN setcap "cap_setuid+ep" /usr/bin/newuidmap \
    && setcap "cap_setgid+ep" /usr/bin/newgidmap
# Add a user that is allowed to map subuids, it will get uid 1000
RUN adduser $user \
    && echo "$user:1:998" > /etc/subuid \
    && echo "$user:1:998" > /etc/subgid
# Add another user that can be used to test groot subuids
RUN groupadd otheruser -g 900 && adduser otheruser -u 900 -g 900
ADD Makefile *.[ch] /tmp
RUN cd /tmp && make && make install
USER $user
